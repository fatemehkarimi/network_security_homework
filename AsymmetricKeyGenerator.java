import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;
import java.util.Random;


public class AsymmetricKeyGenerator{
  private int NUMBER_OF_ASYMMETRIC_KEYS = 7;
  private long num1;
  private long num2;
  private long base;
  private short privateKey;
  private short publicKey;

  //client will use this constructor
  public AsymmetricKeyGenerator(){
    Random rand = new Random();
    int index = rand.nextInt(NUMBER_OF_ASYMMETRIC_KEYS) + 1;
    int count_keys = 0;

    try{
      File f = new File("asymmetric_keys.csv");
      Scanner reader = new Scanner(f);

      while(reader.hasNextLine()){
        String data = reader.nextLine();
        String[] values = data.split(",");
        count_keys += 1;
        if(count_keys == index){
          this.num1 = Integer.parseInt(values[0]);
          this.num2 = Integer.parseInt(values[1]);
          this.privateKey = (short)Integer.parseInt(values[2]);
          this.publicKey = (short)Integer.parseInt(values[3]);
          break;
        }
      }
    } catch(FileNotFoundException e){
      e.printStackTrace();
    }
  }

  public AsymmetricKeyGenerator(long num1, long num2, short puKey, short prKey){
    this.num1 = num1;
    this.num2 = num2;
    publicKey = puKey;
    privateKey = prKey;
  }

  public AsymmetricKeyGenerator(long base, short publicKey){
    this.base = base;
    this.publicKey = publicKey;
  }

  public long getNum1(){
    return this.num1;
  }

  public long getNum2(){
    return this.num2;
  }

  public short getPrivateKey(){
    return this.privateKey;
  }

  public short getPublicKey(){
    return this.publicKey;
  }

  public long getBase(){
    return this.base;
  }

  public void set_Asymmetric_key(long base, short puKey){
    this.base = base;
    this.publicKey = puKey;
  }
}
