import java.io.InputStream;
import java.io.IOException;
import java.io.DataInputStream;
import java.util.Arrays;
import java.nio.ByteBuffer;

public class SecureInputStream extends InputStream{
  private InputStream in;
  private byte[] symmetricKey;
  private long initial_value;
  private boolean startEncrypt;
  private byte[] encryptPlain;
  private byte[] decryptPlain;
  private String message;

  public SecureInputStream(byte[] symmetricKey, InputStream in){
    this.in = in;
    this.symmetricKey = symmetricKey;
    this.startEncrypt = false;
  }

  public void setInitialValue(long IV){
    initial_value = IV;
  }

  public void setStartEncrypt(boolean value){
    startEncrypt = value;
  }

  public void setSymKey(byte[] symKey){
    this.symmetricKey = symKey;
  }

  private byte[] convertToByte(long x){
    ByteBuffer buffer = ByteBuffer.allocate(Long.BYTES);
    buffer.putLong(x);
    return buffer.array();
  }

  public int read() throws IOException{
    if(startEncrypt){
        if(message == null || message.length() == 0){
          if(get_string()){
            --initial_value;
            decryptCBCmode();
            message = new String(decryptPlain);
            message = message.replaceFirst("^\\s*", "");
          }
        }
        if(message != null && message.length() > 0){
          int c = message.charAt(0);
          message = message.substring(1);
          return c;
        }
      return -1;
    }
    return in.read();
  }

  private boolean get_string() {
    DataInputStream dIn = new DataInputStream(in);
    try{
        int length = dIn.readInt();
        if(length > 0){
          encryptPlain = new byte[length];
          dIn.readFully(encryptPlain, 0, length);
          return true;
        }
      }
      catch(IOException x){
        System.out.println("Cant write to socket");
      }
      return false;
  }

  private void decryptCBCmode(){
    BlockCipher cipher = new BlockCipher(symmetricKey);
    byte[] preStage = convertToByte(initial_value);
    decryptPlain = new byte[encryptPlain.length];

    for(int i = 0; i < encryptPlain.length; i += 8){
      cipher.decrypt(encryptPlain, i, decryptPlain, i);
      for(int j = 0; j < 8; ++j){
        decryptPlain[i + j] = (byte)(decryptPlain[i + j] ^ preStage[j]);
        preStage[j] = encryptPlain[i + j];
      }
    }
  }
}
