import java.io.ObjectOutputStream;
import java.io.ObjectInputStream;
import java.io.OutputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.io.IOException;
import java.util.Scanner;
import java.io.DataOutputStream;
import java.io.DataInputStream;
import java.util.Arrays;

public class ChatClient {
  private long initial_value;
  private byte[] privateKey;

  public ChatClient(String username,
		    String serverHost, int serverPort,
		    byte[] clientPrivateKey, byte[] serverPublicKey)
                                                         throws IOException {

    SecureSocket sock = new SecureSocket(serverHost, serverPort,
            clientPrivateKey, serverPublicKey);
    OutputStream out = sock.getOutputStream();

    getInitialVector(sock.getInputStream());
    sock.setInitialValue(initial_value);
    boolean success_key_exchanged = startKeyExchange(sock);

    if(!success_key_exchanged){
      System.out.println("server not authenticated");
      System.exit(-1);
    }

    sock.setSymKey(privateKey);
    sendAuth(username, out);

    new ReceiverThread(sock.getInputStream());

    sock.setStartEncrypt(true);

    for(;;){
      int c = System.in.read();
      if(c == -1)    break;
      out.write(c);
      if(c == '\n')    out.flush();
    }
    out.close();
  }

  public static void main(String[] argv){
    String username = argv[0];
    String hostname = (argv.length<=1) ? "localhost" : argv[1];
    try{
      new ChatClient(username, hostname, ChatServer.portNum, null, null);
    }catch(IOException x){
      x.printStackTrace();
    }
  }

  public boolean startKeyExchange(SecureSocket sock) throws IOException{
    //these are public informations of server, known to everyone
    InputStream in = sock.getInputStream();
    OutputStream out = sock.getOutputStream();
    short SERVER_PUBLIC_KEY = 1499;
    long SERVER_BASE = 1004467;
    AsymmetricKeyGenerator self_info = new AsymmetricKeyGenerator();
    AsymmetricKeyGenerator server_info = new AsymmetricKeyGenerator(SERVER_BASE, SERVER_PUBLIC_KEY);
    HandShakeProtocol handShake = new HandShakeProtocol(
      in, out, self_info, server_info);

    boolean success = handShake.handShakeWithServer();
    if(!success){
      return false;
    }
    int ack_msg = handShake.ACKMessage();
    sock.setSymKey(handShake.get_symmetricKey());
    this.privateKey = handShake.get_symmetricKey();
    sock.setStartEncrypt(true);
    out.write(ack_msg);
    out.write('\n');
    sock.setStartEncrypt(false);
    return true;
  }

  private void getInitialVector(InputStream in) throws IOException{
    try{
      ObjectInputStream inObj = new ObjectInputStream(in);
      Object initial_value_obj = inObj.readObject();
      initial_value = (long)initial_value_obj;
    }
    catch(ClassNotFoundException x){
      System.err.println("ClassNotFoundException error");
    }
  }

  private void getPrivateKey(InputStream in) throws IOException{
    DataInputStream dIn = new DataInputStream(in);
    int length = dIn.readInt();
    if(length > 0){
      privateKey = new byte[length];
      dIn.readFully(privateKey, 0, privateKey.length);
    }
  }

  private void sendAuth(String username, OutputStream out) throws IOException {
    // create an AuthInfo object to authenticate the local user,
    // and send the AuthInfo to the server
    Scanner line_reader = new Scanner(System.in);
    System.out.println("please enter your password");
    String password = line_reader.nextLine();
    AuthenticationInfo auth = new AuthenticationInfo(username, password, initial_value, privateKey);
    ObjectOutputStream oos = new ObjectOutputStream(out);
    oos.writeObject(auth);
    oos.flush();
  }

  class ReceiverThread extends Thread {
    // gather incoming messages, and display them

    private InputStream in;

    ReceiverThread(InputStream inStream) {
      in = inStream;
      start();
    }

    public void run() {
      try{
	ByteArrayOutputStream baos;  // queues up stuff until carriage-return
	baos = new ByteArrayOutputStream();
	for(;;){
	  int c = in.read();
	  if(c == -1){
	    spew(baos);
	    break;
	  }
	  baos.write(c);
	  if(c == '\n')    spew(baos);
	}
      }catch(IOException x){ }
    }

    private void spew(ByteArrayOutputStream baos) throws IOException {
      byte[] message = baos.toByteArray();
      baos.reset();
      System.out.write(message);
    }
  }
}
