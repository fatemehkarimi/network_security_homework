public class AsymmetricKeyEncryption {
  private long base;
  private long fi;
  private short privateKey;
  private short publicKey;

  AsymmetricKeyEncryption(long num1, long num2, short puKey, short prKey) {
    privateKey = prKey;
    publicKey = puKey;
    base = num1 * num2;
    fi = (num1 - 1) * (num2 - 1);
  }

  AsymmetricKeyEncryption(long base, short puKey){
    this.base = base;
    this.publicKey = puKey;
  }

  public long encrypt(long text){
    long cipher = 1;
    for(short i = 0; i < publicKey; ++i){
      cipher = cipher * text;
      cipher = cipher % base;
    }
    return cipher;
  }

  public long decrypt(long cipher){
    long text = 1;
    for(short i = 0; i < privateKey; ++i){
      text = text * cipher;
      text = text % base;
    }
    return text;
  }
}
