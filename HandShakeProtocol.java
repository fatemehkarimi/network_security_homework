import java.io.File;
import java.util.Scanner;
import java.util.Random;
import java.nio.ByteBuffer;
import java.io.OutputStream;
import java.io.InputStream;
import java.io.DataOutputStream;
import java.io.DataInputStream;
import java.io.IOException;
import java.util.Arrays;

public class HandShakeProtocol {
  InputStream in;
  OutputStream out;
  AsymmetricKeyGenerator selfInfo;
  AsymmetricKeyGenerator otherInfo;
  AsymmetricKeyEncryption selfEncryptor;
  AsymmetricKeyEncryption otherEncryptor;
  byte[] self_challenge;
  byte[] other_challenge;
  byte[] symmetricKey_byte;//this will be the asymmetric key

  //this constructor is used by client
  public HandShakeProtocol(
      InputStream in, OutputStream out,
      AsymmetricKeyGenerator selfInfo, AsymmetricKeyGenerator otherInfo){
    this.out = out;
    this.in = in;
    this.selfInfo = selfInfo;
    this.otherInfo = otherInfo;

    selfEncryptor = new AsymmetricKeyEncryption(
      selfInfo.getNum1(),
      selfInfo.getNum2(),
      selfInfo.getPublicKey(),
      selfInfo.getPrivateKey()
    );

    otherEncryptor = new AsymmetricKeyEncryption(
      otherInfo.getBase(),
      otherInfo.getPublicKey()
    );
  }

  //this constructor is used by server
  public HandShakeProtocol(
      InputStream in, OutputStream out, AsymmetricKeyGenerator selfInfo){
    this.out = out;
    this.in = in;
    this.selfInfo = selfInfo;
    this.otherInfo = otherInfo;

    selfEncryptor = new AsymmetricKeyEncryption(
      selfInfo.getNum1(),
      selfInfo.getNum2(),
      selfInfo.getPrivateKey(),
      selfInfo.getPublicKey()
    );
  }

  private boolean send_message_to_outstream(long text) throws IOException{
    DataOutputStream dOut = new DataOutputStream(out);
    try{
      dOut.writeLong(text);
    }catch(IOException e){
      System.out.println("cannot write to socket");
      return false;
    }
    return true;
  }

  private long read_message_from_inputstream() throws IOException{
    long text = 0;
    try{
      DataInputStream dIn = new DataInputStream(in);
      text = dIn.readLong();
    }catch(IOException e){
      System.out.println("cannot read from socket");
      return 0;
    }
    return text;
  }

  private byte[] make_challenge(){
    Random rand = new Random();
    // this number is short to avoid heavy computations in CPU.
    short challenge = (short)(rand.nextInt(7) + 2);
    return ByteBuffer.allocate(2).putShort(challenge).array();
  }

  private byte[] public_key_to_byte(){
    short public_key = (short)selfInfo.getPublicKey();
    return ByteBuffer.allocate(2).putShort(public_key).array();
  }

  private int make_initial_message(){
    byte[] challenge = make_challenge();
    byte[] public_key = public_key_to_byte();
    byte[] message = new byte[challenge.length + public_key.length];
    System.arraycopy(challenge, 0, message, 0, challenge.length);
    System.arraycopy(public_key, 0, message, challenge.length, public_key.length);
    int text = ByteBuffer.wrap(message).getInt();
    this.self_challenge = challenge;
    return text;
  }

  private long SYNmessage(){
    long text = make_initial_message();
    long cipher = otherEncryptor.encrypt(text);
    return cipher;
  }

  private long find_base_of_other_publicKey(short publicKey){
    /*NOTE: acctualy, client should send its (base, publicKey) to server
      but because sending (challenge, base, publicKey) needs a lot of computation
      and does not fit in 64bit, client only sends its publicKey and server looks
      up for base in a file.
    */
    try{
      File f = new File("public_key_base.csv");
      Scanner reader = new Scanner(f);
      while(reader.hasNextLine()){
        String data = reader.nextLine();
        String[] values = data.split(",");
        short key = (short)Integer.parseInt(values[0]);
        if(key == publicKey){
          long base = Long.parseLong(values[1]);
          return base;
        }
      }
    }catch(IOException e){
      e.printStackTrace();
      return -1;//error reading file
    }
    return -1;//not found
  }


  //this will return the 2 byte challenge sent by user
  private byte[] analyze_SYNmessage(int syn_msg){
    byte[] syn_byte = ByteBuffer.allocate(4).putInt(syn_msg).array();
    byte[] challenge = new byte[2];
    byte[] publicKey_byte = new byte[2];

    System.arraycopy(syn_byte, 0, challenge, 0, challenge.length);
    System.arraycopy(syn_byte, challenge.length, publicKey_byte, 0, publicKey_byte.length);

    short publicKey = ByteBuffer.wrap(publicKey_byte).getShort();
    long base = find_base_of_other_publicKey(publicKey);

    otherEncryptor = new AsymmetricKeyEncryption(base, publicKey);
    return challenge;
  }

  private long SYNACKMessage(long plain_syn){
    //We know that plain text of Syn message fits in Int.
    this.other_challenge = analyze_SYNmessage((int)plain_syn);
    this.self_challenge = make_challenge();

    this.symmetricKey_byte = new byte[other_challenge.length + self_challenge.length];
    System.arraycopy(self_challenge, 0, symmetricKey_byte, 0, self_challenge.length);
    System.arraycopy(other_challenge, 0, symmetricKey_byte, self_challenge.length, other_challenge.length);


    int plain_synack = ByteBuffer.wrap(symmetricKey_byte).getInt();
    long cipher_symmKey = otherEncryptor.encrypt(plain_synack);
    return cipher_symmKey;
  }

  private boolean analyze_SYNACKmessage(int plain_synack){
    byte[] synack_byte = ByteBuffer.allocate(4).putInt(plain_synack).array();
    this.symmetricKey_byte = synack_byte;
    other_challenge = new byte[2];
    byte[] self_challenge_sent_by_server = new byte[2];
    System.arraycopy(synack_byte, 0, other_challenge, 0, 2);
    System.arraycopy(synack_byte, 2, self_challenge_sent_by_server, 0, 2);
    if(Arrays.equals(this.self_challenge, self_challenge_sent_by_server)){
      return true;
    }
    return false;
  }

  public int ACKMessage(){
    int ack = ByteBuffer.wrap(this.other_challenge).getShort();
    return ack + 1;
  }

  public int expectedACKMessage(){
    int ack = ByteBuffer.wrap(this.self_challenge).getShort();
    return ack + 1;
  }

  public byte[] get_symmetricKey(){
    return this.symmetricKey_byte;
  }

  public boolean handShakeWithServer(){
    boolean success;
    long syn_msg = SYNmessage();
    try{
      success = send_message_to_outstream(syn_msg);
      if(!success){
        return false;
      }

      long synack_msg = read_message_from_inputstream();
      if(synack_msg == 0){
        return false;
      }

      //we are sure that the plain syn-ack will fit in 4byte Int
      int plain_synack = (int)selfEncryptor.decrypt(synack_msg);
      success = analyze_SYNACKmessage(plain_synack);
      if(!success){
        return false;
      }

    }catch(IOException e){
      return false;
    }

    return true;
  }

  public boolean handShakeWithClient(){
    boolean success;
    try{
      long syn_msg = read_message_from_inputstream();
      if(syn_msg == 0){
        return false;//failed to read syn_msg
      }
      long plain_syn = selfEncryptor.decrypt(syn_msg);

      long synack_msg = SYNACKMessage(plain_syn);
      success = send_message_to_outstream(synack_msg);
      if(!success){
        return false;
      }
    }catch(IOException e){
      return false;
    }
    return true;
  }
}
