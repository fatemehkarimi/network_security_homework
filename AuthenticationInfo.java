import java.io.Serializable;
import java.nio.charset.StandardCharsets;
import java.nio.ByteBuffer;
import java.util.Arrays;
import java.io.File;
import java.util.Scanner;

public class AuthenticationInfo implements Serializable {
  // The fields of this object are set by the client, and used by the
  // server to validate the client's identity.  The client constructs this
  // object (by calling the constructor).  The client software (in another
  // source code file) then sends the object across to the server.  Finally,
  // the server verifies the object by calling isValid().

  private String username;
  private byte[] encryptHash;
  private long IV;
  private byte[] privateKey;

  public AuthenticationInfo(String name, String password, long initial_value, byte[] key){
    // This is called by the client to initialize the object.
    username = name.toString();
    IV = initial_value;
    privateKey = key;

    byte[] hashResult = hashPassword(password);
    encryptCBCmode(hashResult);
  }

  private byte[] hashPassword(String password){
    HashFunction password_hasher = new HashFunction();
    byte[] pass_byte = password.getBytes();
    password_hasher.update(pass_byte);
    byte[] hash = password_hasher.digest();
    byte[] padding = new byte[]{0, 0, 0, 0};
    byte[] hashResult = new byte[24];

    System.arraycopy(hash, 0, hashResult, 0, hash.length);
    System.arraycopy(padding, 0, hashResult, hash.length, padding.length);

    return hashResult;
  }

  private byte[] convertToByte(long x){
    ByteBuffer buffer = ByteBuffer.allocate(Long.BYTES);
    buffer.putLong(x);
    return buffer.array();
  }

  private void encryptCBCmode(byte[]hashResult){
    BlockCipher cipher = new BlockCipher(privateKey);
    byte[] preStage = convertToByte(IV);
    byte[] xoredHash = new byte[hashResult.length];
    encryptHash = new byte[hashResult.length];

    for(int i = 0; i < hashResult.length; i += 8){
      for(int j = 0; j < 8; ++j){
        xoredHash[i + j] = (byte)(hashResult[i + j] ^ preStage[j]);
      }
      cipher.encrypt(xoredHash, i, encryptHash, i);
      for(int j = 0; j < 8; ++j){
        preStage[j] = encryptHash[i + j];
      }
    }
  }

  private byte[] decryptCBCmode(){
    BlockCipher cipher = new BlockCipher(privateKey);
    byte[] preStage = convertToByte(IV);
    byte[] decryptHash = new byte[encryptHash.length];

    for(int i = 0; i < encryptHash.length; i += 8){
      cipher.decrypt(encryptHash, i, decryptHash, i);
      for(int j = 0; j < 8; ++j){
        decryptHash[i + j] = (byte)(decryptHash[i + j] ^ preStage[j]);
        preStage[j] = encryptHash[i + j];
      }
    }
    return decryptHash;
  }

  public boolean isValid() {
    // This is called by the server to make sure the user is who he/she
    // claims to be.

    // Presently, this is totally insecure -- the server just accepts the
    // client's assertion without checking anything.  Homework assignment 1
    // is to make this more secure.
    String TrueUserPassword = "";
    try {
      File passFile = new File("user_passwords.txt");
      Scanner sc = new Scanner(passFile);

      while(sc.hasNextLine()){
        String line = sc.nextLine();
        String[] userPassArray = line.split(", ");
        if(username.equals(userPassArray[0])){
          TrueUserPassword = userPassArray[1];
          break;
        }
      }
    }
    catch(java.io.FileNotFoundException e){
      System.err.println("cannot find password file");
      return false;
    }

    if(TrueUserPassword.equals("")){//user does not exists
      return false;
    }

    byte[] hashUserSentPassword = decryptCBCmode();
    byte[] hashTrueUserPassword = hashPassword(TrueUserPassword);

    return Arrays.equals(hashUserSentPassword, hashTrueUserPassword);
  }

  public String getUserName() {
    return isValid() ? username : null;
  }
}
