import java.io.OutputStream;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.Arrays;
import java.io.DataOutputStream;


public class SecureOutputStream extends OutputStream{
  private boolean startEncrypt;
  private OutputStream out;
  private byte[] symmetricKey;
  private long initial_value;
  private byte[] encryptPlain;
  private String message;

  public SecureOutputStream(byte[] symmetricKey, OutputStream out) {
    this.out = out;
    this.symmetricKey = symmetricKey;
    this.startEncrypt = false;
    this.message= "";
  }

  public void setInitialValue(long IV){
    initial_value = IV;
  }

  public void setStartEncrypt(boolean value){
    this.startEncrypt = value;
  }

  public void setSymKey(byte[] symKey){
    this.symmetricKey = symKey;
  }

  public void write(int c) throws IOException{
    if(startEncrypt){
      message = message + (char)c;
      if(c == '\n'){
        --initial_value;
        add_padding();
        encryptCBCmode();
        send_message();
        message = "";
      }
    }
    else
      out.write(c);
  }

  private byte[] convertToByte(long x){
    ByteBuffer buffer = ByteBuffer.allocate(Long.BYTES);
    buffer.putLong(x);
    return buffer.array();
  }

  private void add_padding(){
    if(message.length() % 8 == 0){
      return;
    }

    int len = 8 - (message.length() % 8);
    char[] padds = new char[len];
    Arrays.fill(padds, ' ');
    String padding = new String(padds);
    message = padding + message;
  }

  public void encryptCBCmode(){
    BlockCipher cipher = new BlockCipher(symmetricKey);
    byte[] byte_message = message.getBytes();
    byte[] preStage = convertToByte(initial_value);
    byte[] xoredPlain = new byte[byte_message.length];
    encryptPlain = new byte[byte_message.length];

    for(int i = 0; i < byte_message.length; i += 8){
      for(int j = 0; j < 8; ++j){
        xoredPlain[i + j] = (byte)(byte_message[i + j] ^ preStage[j]);
      }
      cipher.encrypt(xoredPlain, i, encryptPlain, i);
      for(int j = 0; j < 8; ++j){
        preStage[j] = encryptPlain[i + j];
      }
    }
  }

  private void send_message() {
    DataOutputStream dOut = new DataOutputStream(out);
    try{
      dOut.writeInt(encryptPlain.length);
      dOut.write(encryptPlain);
    }
    catch(IOException x){
      System.out.println("Cant write to socket");
    }
  }
}
